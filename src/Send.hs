{-# LANGUAGE OverloadedStrings #-}

module Send
( send
, toByteString
) where

import Network.Wreq
import Control.Lens
import qualified Data.ByteString.Char8 as C
import qualified Data.ByteString.Lazy.Char8 as P
import qualified Data.Maybe as M


send :: [[FormParam]] -> IO ()
send [] = return ()
send (x:xs) = do
  reply <- post "http://ix.io" x
  let url = P.unpack $ M.fromJust $ reply ^? responseBody
  putStr url
  appendFile "ix.pastes" $ "name" ++ " " ++ url
  send xs

toByteString :: String -> C.ByteString
toByteString = C.pack
-- toByteString' = C.concat . L.toChunks . E.encodeUtf8 . B.pack
