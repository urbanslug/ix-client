import System.Console.GetOpt.Simple
import System.FilePath.Posix
import qualified Data.Maybe as M
import qualified Data.Text.Lazy.IO as Q
import qualified Network.Wreq as N
import qualified Data.List as D
import qualified Data.Map as L
import Control.Monad
import Send


options :: [(FlagMaker, String, Mode, String)]
options = [ (arg, "name",     Optional,   "Filename. The name of the file")
          , (arg, "login",    Optional,   "This takes  your date")
          , (arg, "token",    Optional,   "Password. Required if login.")
          , (arg, "remove",   Optional,   "id to delete. login required.")
          , (arg, "id",       Optional,   "id to replace. login required.")
          ]

          
flags :: [String]
flags = ["name", "login", "password", "remove", "id"]

main ::  IO ()
main = do
  (opts, args) <- getUsingConf options ["file"]
  request <- formRequest args flags  opts
  putStrLn "Generating pastes..."
  send request
  return ()



parseOpts :: [String] -> Options -> [N.FormParam]
parseOpts [] _ = []
parseOpts (opt:xs) opts =
  let value = (L.lookup opt opts)
  in if (M.isJust value) == True
     then (toByteString (opt ++ ":1") N.:= toByteString (M.fromJust value)) : parseOpts xs opts
     else parseOpts xs opts


formRequest :: [FilePath] -> [String] -> Options -> IO [[N.FormParam]]
formRequest _ [] _ = return []
formRequest [] opt opts =  do { return $ parseOpts opt opts : [] }
formRequest files opt opts = forM files $ \file -> do
  let index = show (1 + (M.fromJust (D.elemIndex file files)))
  contentsAsText <- Q.readFile file
  return ([ toByteString ("f:" ++ index) N.:=  contentsAsText
          , toByteString ("ext:" ++ index) N.:= toByteString (takeExtension file)
          ] ++ (parseOpts opt opts))

