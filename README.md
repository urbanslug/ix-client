ix.io client in haskell
-----------------------


Issues:
-------

The parser for command line arguments reads only one argument per flag so filetype/extension will be autodetected by the program. There is no -t flag to manually set extension e.g -t file1.py

login and token(password) go hand in hand. There is nothing in the program to stop you from entering one without the other.


Usage:
------
If ix-client is not in $PATH (if in $PATH replace ./ix-client with ix-client)

1. Just pasting two files     `./ix-cleint file1 file2`
2. Login                      `./ix-client -l username -t password` 
3. Delete (login required)    `./ix-client -l username -t password -r <id-of-paste-to-delete> `
4. id (login required)        `./ix-client -l username -t password  -i <id-to-replace>`


